from guardian import GuardState, GuardStateDecorator
import ISC_library
import ISC_GEN_STATES
import json
import time
import numpy
import lscparams
import cdsutils
import timeout_utils as timeout

##################################################
# Prep file path for saving offsets
##################################################
# Jnote: is this necessary?  It's used in DIFF, but not in COMM?
alsCommParamsPath = '/opt/rtcds/userapps/trunk/als/common/guardian/alsCommParams.dat'
with open(alsCommParamsPath, 'r') as alsCommParamFile:
    alsCommParamDict = json.load(alsCommParamFile)

##################################################
# STATES:
##################################################
nominal = 'SHUTTERED'


class LOCKLOSS(GuardState):
    request = False

    def run(self):
        return True


class DOWN(GuardState):
    goto = True
    index = 100
    def main(self):
        ezca['ALS-C_REFL_DC_BIAS_GAIN'] = 0

        #prepare the MC path
        ezca.switch('IMC-MCL', 'FM1', 'OFF')
        ezca['SUS-MC2_M3_LOCK_L_LIMIT'] = 400000
        ezca.switch('SUS-MC2_M3_LOCK_L', 'FM6','FM10', 'OFF', 'FM3', 'FM9', 'ON')
        ezca['IMC-MCL_TRAMP'] = 0
        ezca['IMC-MCL_GAIN'] = 1
        ezca['ALS-C_COMM_PLL_INEN'] = 1
        ezca['ALS-C_COMM_PLL_BOOST'] = 0

        #get rid of feedback from COMM
        ezca['IMC-REFL_SERVO_IN2EN'] = 'Off'
        ezca['LSC-REFL_SERVO_IN1EN'] = 'Off'
        ezca['LSC-REFL_SERVO_IN2EN'] = 0
        ezca['LSC-REFL_SERVO_COMBOOST'] = 0
        ezca['LSC-MCL_GAIN'] = 0
        ezca.switch('LSC-MCL', 'FM4', 'FM5', 'FM7', 'OFF')
        ezca['LSC-REFL_SERVO_COMCOMP'] = 0
        ezca['LSC-REFL_SERVO_IN2GAIN'] = -32
        ezca['LSC-REFL_SERVO_FASTGAIN'] = 0
        ezca['LSC-REFL_SERVO_SLOW_GAIN'] = 1
        ezca['ALS-C_COMM_VCO_CONTROLS_ENABLE'] = 0  #turn off VCO frequency servo so you don't have two servos trying to feedback to the VCO
        ezca['ALS-C_COMM_VCO_TUNEOFS'] = 0  #get rid of any random offsets from times the frequency servo ran in the past. 
        ezca['ALS-C_COMM_PLL_PRESET'] = 'No'

class PREP_FOR_HANDOFF(GuardState):
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        ezca.switch('SUS-MC2_M3_LOCK_L', 'FM6','FM10', 'OFF', 'FM3', 'FM9', 'ON')
        # set up summing junction
        ezca['LSC-REFL_SUM_B_IN1GAIN'] = 0
        ezca['LSC-REFL_SUM_B_IN1EN'] = 1
        ezca['LSC-REFL_SUM_B_IN1POL'] = 1
        ezca['LSC-REFL_SUM_B_IN2EN'] = 0

        ISC_library.outrix.put('MC2',[],0)
        ISC_library.outrix['MC2', 'MCL'] = 1

        # set up CARM
        ezca['LSC-MCL_TRIG_THRESH_ON'] = -1
        ezca['LSC-MCL_TRIG_THRESH_OFF'] = -1
        ezca['LSC-CONTROL_ENABLE'] = 1
        ezca.get_LIGOFilter('LSC-MCL').only_on('INPUT','FM8', 'OUTPUT', 'DECIMATION')
        ezca['LSC-MCL_GAIN'] = 240

        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0.1
        ISC_library.intrix['MCL', 'REFLSERVO_SLOW'] = 0.1

        #commenting out July 29 2020
        #ezca['ALS-C_COMM_VCO_CONTROLS_ENABLE'] = 1

        self.timer['wait'] = 5 # ensure locked 5 seconds before starting handoff

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if abs(ezca['ALS-C_COMM_VCO_TUNEOFS']) + 0.1 > ezca['ALS-C_COMM_VCO_TUNELIMIT']:
            ezca['ALS-C_COMM_VCO_CONTROLS_CLEARINT'] = 1
            time.sleep(0.1)
            ezca['ALS-C_COMM_VCO_CONTROLS_CLEARINT'] = 0
        elif ezca['ALS-C_COMM_A_DEMOD_RFMON'] < -20: # was -9. 31July2019 JCD
            notify('COMM beatnote is low!')
        elif self.timer['wait'] and abs(ezca['ALS-C_COMM_PLL_CTRLMON']) < 9.9: # if COMM PLL is not railed  abs(ezca['LSC-REFL_SUM_B_IN1MON']) < 9.9
            ISC_library.intrix.load()
            return True


class HANDOFF_PART1(GuardState):
    request = False
    index = 1

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):

        #prepare refl board
        ezca['LSC-REFL_SERVO_IN2GAIN'] = lscparams.COMM_LSC_IN2GAIN
        ezca['LSC-REFL_SERVO_FASTGAIN'] = lscparams.COMM_LSC_FASTGAIN
        ezca['LSC-REFL_SERVO_IN2EN'] = 1

        #prepare AO path
        ezca['IMC-REFL_SERVO_IN2POL'] = 0
        ezca['IMC-REFL_SERVO_IN2GAIN'] = lscparams.COMM_IMC_IN2GAIN # CC: Jan 23 2019, was -6, changed as a part of offloading 6 dB of gain to IMC FASTGAIN
        ezca['IMC-REFL_SERVO_IN2EN'] = 1

        #prepare MC2
        ezca['SUS-MC2_M3_LOCK_L_LIMIT'] = 6400000
        ezca['IMC-MCL_TRAMP'] = 2
        self.timer['wait'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if self.timer['wait']:
            return True


class HANDOFF_PART2(GuardState):
    request = False
    index = 2

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        #turn down MCL DC gain
        ezca.switch('IMC-MCL','FM1', 'ON')

        #turn up COMM gain
        while (ezca['LSC-REFL_SERVO_IN2GAIN'] < lscparams.COMM_HANDOFF_PART2_LSC_IN2GAIN): # 17 dB
            ezca['LSC-REFL_SERVO_IN2GAIN'] += 1
            time.sleep(0.1)

        #low frequency boost
        ezca.switch('LSC-MCL', 'FM5', 'ON')
        self.timer['wait2'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if self.timer['wait2']:
            return True


class HANDOFF_PART3(GuardState):
    request = False
    index = 3

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        # turn the MCL feedback off
        ezca['IMC-MCL_GAIN'] = 0

        # CARM boost up
        ezca.switch('LSC-MCL', 'FM4', 'ON')
        ezca.switch('LSC-MCL', 'FM7', 'ON') # Even more low freq gain, JCD 13Dec2018 Seems good with high useism
        ezca['LSC-MCL_GAIN'] = 300 # A bit more gain, JCD 13Dec2018
        self.timer['wait2'] = 2

        if self.timer['wait2']:
            log('CARM boost up success')
        ezca.switch('SUS-MC2_M3_LOCK_L','FM3', 'OFF') #gm hy tvo commenting out because comm keeps losing lock 20181127

        #COMM PLL boost
        ezca['ALS-C_COMM_PLL_BOOST'] = 1 #gm hy tvo commenting out because comm keeps losing lock 20181127

        self.timer['wait'] = 5

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if self.timer['wait']:
            # turn on VCO frequency stabilization servo
            ezca['ALS-C_COMM_VCO_CONTROLS_ENABLE'] = 1
            ezca['ALS-C_COMM_PLL_PRESET'] = 'No'
            return True


class LOCKED(GuardState):
    index = 5

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        return True


class PREP_SEARCH(GuardState):
    """Check that our QPD offset is acceptable. Otherwise, we will
    have a lower/higher signal than we actually do and confuse our
    static threshold values. A better fix would be to run the dark
    offset script to adjust the qpd seg offsets, but this seems to
    happen often and just needs a minor change that I think this is
    an okay solution - TJS May 2 2023
    """
    index = 20
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        self.acceptable_dark = 0.05
        # Set the VCO frequency somewhere away incase we are already getting light
        ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY'] = 78900000
        self.timer['freq_move'] = 5
        self.freq_set = False

    def run(self):
        if self.timer['freq_move'] and not self.freq_set:
            self.freq_set = True
            return False
        elif self.freq_set:
            log('Getting 5sec of "dark" time')
            self.avg = timeout.call_with_timeout(cdsutils.avg, 5, 'H1:LSC-TR_X_NORM_INMON')
            if abs(self.avg) > self.acceptable_dark:
                log('Need to reset TR X QPD B offset')
                # I think just taking the average here should be good enough
                dark_avg = timeout.call_with_timeout(cdsutils.avg, 5, 'H1:LSC-TR_X_QPD_B_SUM_INMON')
                ezca['LSC-TR_X_QPD_B_SUM_OFFSET'] = -dark_avg.round(3)
                return True
            else:
                return True


class FAST_SEARCH(GuardState):
    index = 21
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        self.freqChan     = 'ALS-C_COMM_VCO_FREQUENCY'
        self.freqSetChan  = 'ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY'
        self.freqDiffChan = 'ALS-C_COMM_VCO_CONTROLS_DIFFFREQUENCY'
        self.trChan       = 'LSC-TR_X_NORM_INMON'
        self.trThresh     = 0.07   # Changed from 0.22 now that we have the prep search state - TJS May 4 2023
        #self.locations    = [78898350, 78935686]
        #self.locations = [78893844, 78931180]
        self.locations = [78893614, 78931180] #glm updated the first frequency 20210331
        self.counter       = 0
        self.timer['wait'] = 0
        self.found_flag = 0

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if ezca['ALS-C_COMM_VCO_ERROR_FLAG']:
            notify('COMM VCO error')
            ezca['ALS-C_COMM_VCO_CONTROLS_CLEARINT'] = 1
            time.sleep(0.1)
            ezca['ALS-C_COMM_VCO_CONTROLS_CLEARINT'] = 0
            return False

        #before moving anything  check that you don't already have IR
        if ezca[self.trChan] > self.trThresh and self.counter == 0:
            self.timer['wait'] = 5
            self.counter += 1
        elif self.counter == 0:
            self.counter += 1

        if self.timer['wait'] and self.counter == 1:
            if ezca[self.trChan] > self.trThresh:
                return 'FINE_TUNE_IR'
            else:
                ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCYOFFSET'] = 0
                ezca[self.freqSetChan] = self.locations[0]
                self.timer['wait'] = 1 # to let comparator catch up
                self.counter += 1

        if ezca[self.freqDiffChan] < 100 and self.counter >= 2 and self.timer['wait'] and not self.found_flag:
            # keep doing this until return something
            if ezca[self.trChan] > self.trThresh:
                # Wait for 5 seconds to allow the diff freq to come down
                log('Found something sleeping 5 seconds to verify')
                self.timer['wait'] = 5
                self.found_flag = True

            elif abs(ezca[self.freqDiffChan]) < 5:
                if self.counter == 2:
                    log('nothing found here, moving to second frequency')
                    ezca[self.freqSetChan] = self.locations[1]
                    self.counter += 1
                    self.timer['wait'] = 2 # let comparator catch up

                else:
                    log('nothing found here, moving to full search')
                    return True

        if self.found_flag and self.timer['wait']:
            # Check that the we are actually found
            found_avg = timeout.call_with_timeout(cdsutils.avg, -5, 'H1:LSC-TR_X_NORM_INMON')
            if found_avg > self.trThresh:
                return 'FINE_TUNE_IR'
            else:
                self.found_flag = False
                return False


class SEARCH_FULL_RANGE(GuardState):
    index = 22
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        self.lowFreq        = -37.5e3
        self.highFreq       =  37.5e3
        self.trThresh       =  0.4
        self.freqChan       = 'ALS-C_COMM_VCO_FREQUENCY'
        self.freqSetChan    = 'ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY'
        self.freqOffsetChan = 'ALS-C_COMM_VCO_CONTROLS_SETFREQUENCYOFFSET'
        self.freqDiffChan   = 'ALS-C_COMM_VCO_CONTROLS_DIFFFREQUENCY'
        self.trChan         = 'LSC-TR_X_NORM_INMON'
        self.coarseSearch   = True
        self.smallStepCounter = 0
        # Check that is not already on resonance
        if ezca[self.trChan] > self.trThresh:
            log('exiting from main before changing the frequency')
            return 'FINE_TUNE_IR'
        # Go to same starting location each time
        ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY'] = 78920000
        ezca[self.freqOffsetChan] = self.lowFreq
        ezca['ALS-C_COMM_VCO_CONTROLS_ENABLE'] = 'On'
        time.sleep(1)
        while ezca[self.freqDiffChan] > 5e2:
            time.sleep(0.5)
        # Check that it is not already on resonaonce
        if ezca[self.trChan] > self.trThresh:
            log('exiting from main after changing the frequency')
            return 'FINE_TUNE_IR'

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        if ezca['ALS-C_TRX_A_LF_OUTPUT'] < 0.75:
            notify('check green alignment!!!')
            pass
        if ezca['ALS-C_COMM_VCO_ERROR_FLAG']:
            notify('COMM VCO ERROR')
        currentFreqOffset = ezca[self.freqOffsetChan]
        if currentFreqOffset < self.highFreq+1e3 and currentFreqOffset >= self.lowFreq-1e3:
            freqData = []
            trData = []
            if self.coarseSearch == True:
                ezca[self.freqOffsetChan] += 2e3
                diffTol = 100
                time.sleep(0.2)
            elif self.coarseSearch == False:
                if currentFreqOffset < 3000 and currentFreqOffset >= -3000:
                    ezca[self.freqOffsetChan] -= 300
                    self.smallStepCounter += 1
                    diffTol = 100
                    time.sleep(0.2)
                else:
                    log('Fine ssearch Failed!')
                    return 'NO_IR_FOUND'
            while abs(ezca[self.freqDiffChan]) > diffTol:
                freqData.append(ezca[self.freqChan])
                trData.append(ezca[self.trChan])
                time.sleep(0.01)
            if trData and max(trData) > self.trThresh:
                goodFreq = freqData[numpy.argmax(trData)]
                ezca[self.freqSetChan] = goodFreq
                ezca[self.freqOffsetChan] = 0
                log(max(trData))
                log(goodFreq)
                log('Going to good frequency.')
                time.sleep(1)
                if ezca[self.trChan] >= self.trThresh:
                    return 'FINE_TUNE_IR'
                if self.coarseSearch == True:
                    log('Moving on to the fine search.')
                    ezca[self.freqOffsetChan] = 2000
                    self.coarseSearch = False
                    freqData = []
                    trData = []
                else:
                    return 'FINE_TUNE_IR'
        else:
            log('Search reached end of range.')
            return 'NO_IR_FOUND'
        if self.smallStepCounter > 80:
            log('Ran out of juice during the fine search.')
            return 'NO_IR_FOUND'


FINE_TUNE_IR = ISC_GEN_STATES.gen_TUNE_IR_BETTER(3001, 50, 2, 0.65, 'LSC-TR_X_NORM_OUTPUT',
                                  'ALS-C_COMM_VCO_CONTROLS_SETFREQUENCYOFFSET', 2, 1, ThreshLow=0.055)


class NO_IR_FOUND(GuardState):
    index = 25
    request = False
    redirect = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        TR=ezca['LSC-TR_X_NORM_INMON']
        log(TR)
        if ezca['LSC-TR_X_NORM_INMON'] > 0.7 and abs(ezca['ALS-C_COMM_VCO_CONTROLS_DIFFFREQUENCY']) < 10:
            return True
        elif ezca.read('GRD-ALS_COMM_TARGET', as_string=True) == 'FAST_SEARCH':
            return True # So that you can chose to have it re-try
        else:
            notify('Search for IR Resonance by hand')


class IR_FOUND(GuardState):
    index = 30

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def main(self):
        #move the set freuquency so that the offset can be zero when the arm is on resonance
        self.StartingOffset = ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCYOFFSET']
        self.StartingSetFrequency = ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY']
        ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCY'] = self.StartingSetFrequency - self.StartingOffset
        ezca['ALS-C_COMM_VCO_CONTROLS_SETFREQUENCYOFFSET'] = 0
        # Save the new optimal set frequency into the parameters file
        alsCommParamDict['commOffset'] = self.StartingSetFrequency - self.StartingOffset
        with open(alsCommParamsPath, 'w+') as alsCommParamFile:
            json.dump(alsCommParamDict, alsCommParamFile)

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN'])
    def run(self):
        return True


class SHUTTERED(GuardState):
    index = 50
    goto = True
    
class IDLE(GuardState):
    index = 51
    goto = True


##################################################
# STATES:
##################################################

edges = [
    ('DOWN',                 'DOWN'),#adding an edge from DOWN to DOWN, this way when we are in DOWN and ISC_LCOK requests down, the main will be run again.
    ('DOWN',                'PREP_FOR_HANDOFF'),
    ('PREP_FOR_HANDOFF',    'HANDOFF_PART1'),
    ('HANDOFF_PART1',       'HANDOFF_PART2'),
    ('HANDOFF_PART2',       'HANDOFF_PART3'),
    ('HANDOFF_PART3',       'LOCKED'),
    ('LOCKED',              'PREP_SEARCH'),
    ('PREP_SEARCH',         'FAST_SEARCH'),
    ('FAST_SEARCH',         'SEARCH_FULL_RANGE'),
    ('SEARCH_FULL_RANGE',   'FINE_TUNE_IR'),
    ('FINE_TUNE_IR',        'IR_FOUND'),
    ('NO_IR_FOUND',         'IR_FOUND'),
    ('NO_IR_FOUND',         'PREP_SEARCH'),
    ('IR_FOUND',            'LOCKED'),
    ('IR_FOUND',            'SHUTTERED'),
    ('IR_FOUND',            'IDLE'),
    ]
